package view;

import controller.Controller;
import controller.ControllerImplement;
import model.Flower;

import java.util.*;

public class View {

  private Controller controller;
  private Map<String, String> menu;
  private Map<String, Printable> methodsMenu;
  private static Scanner input = new Scanner(System.in);

  public View() {
    controller = new ControllerImplement();
    menu = new LinkedHashMap<>();
    menu.put("1", "  1 - Show our craft 'BloodyMarry' bouquet flowers sorted by price");
    menu.put("2", "  2 - Show our craft 'For Lovely Mother' bouquet flowers sorted by price");
    menu.put("3", "  3 - Show our craft 'Wedding' bouquet flowers sorted by price");
    menu.put("Q", "  Q - exit");

    methodsMenu = new LinkedHashMap<>();
    methodsMenu.put("1", this::pressButton1);
    methodsMenu.put("2", this::pressButton2);
    methodsMenu.put("3", this::pressButton3);
  }

  private void pressButton1() {
    List<Flower> flowers;
    controller.createBloodyMarryBouquet();
    flowers = controller.sortByPriceAsc();
    flowers.forEach(System.out::println);
  }

  private void pressButton2() {
    List<Flower> flowers;
    controller.createForLovelyMotherBouquet();
    flowers = controller.sortByPriceAsc();
    flowers.forEach(System.out::println);
  }

  private void pressButton3() {
    List<Flower> flowers;
    controller.createWeddingBouquet();
    flowers = controller.sortByPriceAsc();
    flowers.forEach(System.out::println);
  }

  private void outputMenu() {
    System.out.println("\nMENU:");
    for (String str : menu.values()) {
      System.out.println(str);
    }
  }

  public void show() {
    String keyMenu;
    do {
      outputMenu();
      System.out.println("Please, select menu point.");
      keyMenu = input.nextLine().toUpperCase();
      try {
        methodsMenu.get(keyMenu).print();
      } catch (Exception e) {
        System.out.println("Something goes wrong! Try again,Please.");
      }
    } while (!keyMenu.equals("Q"));
  }
}
