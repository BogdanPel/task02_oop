import controller.Controller;
import controller.ControllerImplement;

public class Main {
  public static void main(String[] args) {
    //
      new ControllerImplement().execute();
  }
}
