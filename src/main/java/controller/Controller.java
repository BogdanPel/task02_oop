package controller;

import model.Flower;

import java.util.ArrayList;
import java.util.List;

public interface Controller {
  public ArrayList<Flower> addFlower(int count, String color, String producer, String flowerType);

  public ArrayList<Flower> createBloodyMarryBouquet();

  public ArrayList<Flower> createForLovelyMotherBouquet();

  public ArrayList<Flower> createWeddingBouquet();

  public List<Flower> sortByPriceAsc();

  public List<Flower> sortByPriceDesc();
}
