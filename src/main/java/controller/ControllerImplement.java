package controller;

import model.BusinessLogic;
import model.Flower;
import model.Model;
import view.View;

import java.util.ArrayList;
import java.util.List;

public class ControllerImplement implements Controller {
  private Model model;

  public ControllerImplement() {
    model = new BusinessLogic();
  }

  @Override
  public ArrayList<Flower> addFlower(int count, String color, String producer, String flowerType) {
    return model.addFlower(count, color, producer, flowerType);
  }

  @Override
  public ArrayList<Flower> createBloodyMarryBouquet() {
    return model.createBloodyMarryBouquet();
  }

  @Override
  public ArrayList<Flower> createForLovelyMotherBouquet() {
    return model.createForLovelyMotherBouquet();
  }

  @Override
  public ArrayList<Flower> createWeddingBouquet() {
    return model.createWeddingBouquet();
  }

  @Override
  public List<Flower> sortByPriceAsc() {
    return model.sortByPriceAsc();
  }

  @Override
  public List<Flower> sortByPriceDesc() {
    return model.sortByPriceDesc();
  }

  public void execute() {
    new View().show();
  }
}
