package model;

import java.util.ArrayList;
import java.util.List;

public interface Model {
  public ArrayList<Flower> addFlower(int count, String color, String producer, String flowerType);

  public ArrayList<Flower> createBloodyMarryBouquet();

  public ArrayList<Flower> createForLovelyMotherBouquet();

  public ArrayList<Flower> createWeddingBouquet();

  public List<Flower> sortByPriceAsc();

  public List<Flower> sortByPriceDesc();
}
