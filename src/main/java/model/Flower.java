package model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Flower {
  private String color;
  private String flowerType;
  private int price;
  private String producer;

  public Flower() {}

  public Flower(String flowerType, String color, String producer) {
    this.flowerType = flowerType;
    this.color = color;
    this.producer = producer;
    setPrice();
  }

  @Override
  public String toString() {
    return "Flower{"
        + "color='"
        + color
        + '\''
        + ", flowerSize='"
        + flowerType
        + '\''
        + ", price="
        + price
        + ", producer='"
        + producer
        + '\''
        + '}';
  }

  private void setPrice() {
    switch (flowerType) {
      case "ROSE":
        this.price = 35;
        break;
      case "TULPIN":
        this.price = 40;
        break;
      case "LILY":
        this.price = 45;
        break;
      case "CHAMOMILE":
        this.price = 15;
        break;
      case "CHRYSANTHEMUM":
        this.price = 25;
        break;
    }
    if (!producer.equals("UKR")) {
      this.price += 30;
    }
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (!(o instanceof Flower)) return false;
    Flower flower = (Flower) o;
    return price == flower.price
        && color.equals(flower.color)
        && flowerType.equals(flower.flowerType)
        && producer.equals(flower.producer);
  }

  @Override
  public int hashCode() {
    return Objects.hash(color, flowerType, price, producer);
  }

  public String getColor() {
    return color;
  }

  public int getPrice() {
    return price;
  }

  public String getFlowerType() {
    return flowerType;
  }

  public String getProducer() {
    return producer;
  }
}
