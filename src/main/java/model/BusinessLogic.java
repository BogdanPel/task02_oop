package model;

import java.util.ArrayList;
import java.util.List;

public class BusinessLogic implements Model {
  private Market market;

  public BusinessLogic() {
    market = new Market();
  }

  @Override
  public ArrayList<Flower> addFlower(int count, String color, String producer, String flowerType) {
    return market.addFlower(count, color, producer, flowerType);
  }

  @Override
  public ArrayList<Flower> createBloodyMarryBouquet() {
    return market.createBloodyMarryBouquet();
  }

  @Override
  public ArrayList<Flower> createForLovelyMotherBouquet() {
    return market.createForLovelyMotherBouquet();
  }

  @Override
  public ArrayList<Flower> createWeddingBouquet() {
    return market.createWeddingBouquet();
  }

  @Override
  public List<Flower> sortByPriceAsc() {
    return market.sortByPriceAsc();
  }

  @Override
  public List<Flower> sortByPriceDesc() {
    return market.sortByPriceDesc();
  }
}
