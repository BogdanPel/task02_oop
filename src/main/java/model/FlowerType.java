package model;

public enum FlowerType {
  TULIP(0),
  ROSE(1),
  CHAMOMILE(2),
  LILY(3);
  private int index;

  public int getIndex() {
    return index;
  }

  FlowerType(int i) {
    index = i;
  }
}
