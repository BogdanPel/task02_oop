package model;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Market {
  private ArrayList<Flower> flowers;

  public Market() {
    this.flowers = new ArrayList<>();
  }

  public ArrayList<Flower> addFlower(int count, String color, String producer, String flowerType) {
    for (int i = 0; i < count; i++) {
      flowers.add(new Flower(flowerType, color, producer));
    }
    return flowers;
  }

  public ArrayList<Flower> createBloodyMarryBouquet() {
    addFlower(3, "RED", "PL", "ROSE");
    addFlower(3, "WHITE", "BELGIUM", "ROSE");
    addFlower(1, "BLACK", "BELGIUM", "ROSE");
    addFlower(2, "RED", "PL", "TULPIN");
    addFlower(2, "WHITE", "PL", "TULPIN");
    return flowers;
  }

  public ArrayList<Flower> createForLovelyMotherBouquet() {
    addFlower(5, "WHITE", "UKR", "TULPIN");
    addFlower(3, "YELLOW", "UKR", "TULPIN");
    addFlower(5, "WHITE", "UKR", "CHRYSANTHEMUM");
    addFlower(3, "BLUE", "UKR", "CHRYSANTHEMUM");
    addFlower(5, "WHITE", "UKR", "CHAMOMILE");
    return flowers;
  }

  public ArrayList<Flower> createWeddingBouquet() {
    addFlower(5, "WHITE", "GER", "TULPIN");
    addFlower(3, "YELLOW", "GER", "TULPIN");
    addFlower(5, "WHITE", "UKR", "CHRYSANTHEMUM");
    addFlower(3, "BLUE", "UKR", "CHRYSANTHEMUM");
    addFlower(5, "WHITE", "UKR", "CHAMOMILE");
    addFlower(3, "RED", "BELGIUM", "ROSE");
    addFlower(3, "WHITE", "BELGIUM", "ROSE");
    addFlower(1, "BLACK", "BELGIUM", "ROSE");
    addFlower(2, "RED", "PL", "TULPIN");
    addFlower(2, "WHITE", "PL", "TULPIN");
    return flowers;
  }

  public List<Flower> sortByPriceAsc() {
    return flowers.stream()
        .sorted((o1, o2) -> o2.getPrice() - o1.getPrice())
        .collect(Collectors.toList());
  }

  public List<Flower> sortByPriceDesc() {
    return flowers.stream()
        .sorted(Comparator.comparingInt(Flower::getPrice))
        .collect(Collectors.toList());
  }
}
